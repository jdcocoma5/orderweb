/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.orderweb.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Aprendiz
 */
@Entity
@Table(name = "order_activity")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderActivity.findAll", query = "SELECT o FROM OrderActivity o")
   , @NamedQuery(name = "OrderActivity.findBySerial", query = "SELECT o FROM OrderActivity o WHERE o.serial = :serial")})
public class OrderActivity implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    
    @Basic(optional = false)
    
    @Column(name = "serial",nullable =false)
    private int serial;
    @JoinColumn(name = "id_order", referencedColumnName = "id_order", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Order order1;
    @JoinColumn(name = "id_activity", referencedColumnName = "id_activity", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Activity activity;

    public OrderActivity() {
    }

    
    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public Order getOrder1() {
        return order1;
    }

    public void setOrder1(Order order1) {
        this.order1 = order1;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
    
}
