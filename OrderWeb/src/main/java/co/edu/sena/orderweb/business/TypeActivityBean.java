/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.TypeActivity;
import co.edu.sena.orderweb.persistence.ITypeActivityDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class TypeActivityBean implements TypeActivityBeanLocal {

    @EJB
    private ITypeActivityDAO typeActivityDAO;
    
    public void validate(TypeActivity typeActivity) throws Exception
    {
        if(typeActivity == null)
        {
            throw new Exception("El tipo de actividad es nulo");
        }
        
        if(typeActivity.getDescription().isEmpty())
        {
            throw new Exception("La descripción es obligatoria");
        }
    }

    @Override
    public void insert(TypeActivity typeActivity) throws Exception {
        validate(typeActivity);  
        typeActivityDAO.insert(typeActivity);
    }

    @Override
    public void update(TypeActivity typeActivity) throws Exception {
        validate(typeActivity);
        TypeActivity oldTypeActivity= typeActivityDAO.findById(typeActivity.getIdType());
        if(oldTypeActivity == null)
        {
            throw new Exception("No existe un tipo de actividad con ese Id");
        }
        
        oldTypeActivity.setDescription(typeActivity.getDescription()); 
        typeActivityDAO.update(oldTypeActivity);
    }

    @Override
    public void delete(TypeActivity typeActivity) throws Exception {
        if(typeActivity == null)
        {
            throw new Exception("El tipo de actividad es nulo");
        }
        
        if(typeActivity.getIdType() == 0)
        {
            throw new Exception("El Id es obligatorio");
        }        
        
        TypeActivity oldTypeActivity= typeActivityDAO.findById(typeActivity.getIdType());
        if(oldTypeActivity == null)
        {
            throw new Exception("No existe un tipo de actividad con ese Id");
        }
        
        typeActivityDAO.delete(oldTypeActivity);
    }

    @Override
    public TypeActivity findById(Integer idType) throws Exception {
        if(idType == 0)
        {
            throw new Exception("El Id es obligatorio");
        } 
        
        return typeActivityDAO.findById(idType);
    }

    @Override
    public List<TypeActivity> findAll() throws Exception {
        return typeActivityDAO.findAll();
    }
    
    
}
