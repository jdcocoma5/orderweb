/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Causal;
import co.edu.sena.orderweb.persistence.ICausalDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class CausalBean implements CausalBeanLocal {

    @EJB
    private ICausalDAO causalDAO;

    public void validate(Causal causal) throws Exception
    {
        if(causal == null)
        {
            throw new Exception("La causa es nula");
        }
        
        if(causal.getDescription().isEmpty())
        {
            throw new Exception("La descripción es obligatoria");
        }
    }
    
    @Override
    public void insert(Causal causal) throws Exception {
        validate(causal);  
        causalDAO.insert(causal);
    }

    @Override
    public void update(Causal causal) throws Exception {
        validate(causal);
        Causal oldCausal= causalDAO.findById(causal.getIdCausal());
        if(oldCausal == null)
        {
            throw new Exception("No existe una causal con ese Id");
        }
        
        oldCausal.setDescription(causal.getDescription()); 
        causalDAO.update(oldCausal);
    }

    @Override
    public void delete(Causal causal) throws Exception {
        if(causal == null)
        {
            throw new Exception("La causa es nula");
        }
        
        if(causal.getIdCausal() == 0)
        {
            throw new Exception("El Id es obligatorio");
        }        
        
        Causal oldCausal= causalDAO.findById(causal.getIdCausal());
        if(oldCausal == null)
        {
            throw new Exception("No existe una causal con ese Id");
        }
        
        causalDAO.delete(oldCausal);
    }

    @Override
    public Causal findById(Integer idCausal) throws Exception {
        if(idCausal == 0)
        {
            throw new Exception("El Id es obligatorio");
        }
        
        return causalDAO.findById(idCausal);
    }

    @Override
    public List<Causal> findAll() throws Exception {
        return causalDAO.findAll();
    }
    
    
}
