/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.orderweb.persistence;


import co.edu.sena.orderweb.model.UserLogin;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface IUserDAO {
    public UserLogin findById(String username) throws Exception;
    public List<UserLogin> findAll() throws Exception;
    public UserLogin findByRole(String role) throws Exception;
}
