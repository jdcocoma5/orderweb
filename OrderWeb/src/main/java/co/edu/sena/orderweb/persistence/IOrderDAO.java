/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.orderweb.persistence;


import co.edu.sena.orderweb.model.Order1;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface IOrderDAO {
    public void insert(Order1 order1) throws Exception;
    public void update(Order1 order1) throws Exception;
    public void delete(Order1 order1) throws Exception;
    public Order1 findById(Integer idOrder) throws Exception;
    public List<Order1> findAll() throws Exception;
}
