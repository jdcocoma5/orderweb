/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/SessionLocal.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Causal;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface CausalBeanLocal {
    public void insert(Causal causal) throws Exception;
    public void update(Causal causal) throws Exception;
    public void delete(Causal causal) throws Exception;
    public Causal findById(Integer idCausal) throws Exception;
    public List<Causal> findAll() throws Exception;
}
