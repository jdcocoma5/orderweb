/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.orderweb.view;

import co.edu.sena.orderweb.business.UserBeanLocal;

import co.edu.sena.orderweb.model.UserLogin;
import co.edu.sena.orderweb.utils.Constants;
import co.edu.sena.orderweb.utils.MessageUtils;
import java.io.IOException;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.component.password.Password;
import org.primefaces.component.selectonemenu.SelectOneMenu;

/**
 *
 * @author Aprendiz objetivo: administrar la vista del login
 */
public class LoginView implements Serializable {

    private UserLogin user;
    private SelectOneMenu selectUsuario;
    private Password password;

    @EJB
    private UserBeanLocal userBean;

    @Inject
    private HttpServletRequest request;

    /**
     * Creates a new instance of UserView
     */
    public LoginView() {
    }

    public UserLogin getUser() {
        return user;
    }

    public void setUser(UserLogin user) {
        this.user = user;
    }

    public SelectOneMenu getSelectUsuario() {
        return selectUsuario;
    }

    public void setSelectUsuario(SelectOneMenu selectUsuario) {
        this.selectUsuario = selectUsuario;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public void clear() {
        selectUsuario.setValue("");
        password.setValue("");
    }

    public void login() {
        try {
            String page = "";
            //ejecuto el login
            user = userBean.login(selectUsuario.getValue().toString(), password.getValue().toString());

            if ("ADMINISTRADOR".equals(user.getRole())) {
                page = "administrador/index.xhtml";
            } else { //Supervisor
                page = "supervisor/index.xhtml";
            }

            // guarda en la session usuario
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
            FacesContext.getCurrentInstance().getExternalContext().redirect(page);

        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }

    public void signOut() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            String path = ((ServletContext) context.getContext()).getContextPath();
            context.redirect(path + "?faces-redirect=true");
        } catch (IOException e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }

    public void validateSession() {
        try {
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            user = (UserLogin) context.getSessionMap().get("user");

            String url = request.getRequestURL().toString();
            String path = ((ServletContext) context.getContext()).getContextPath(); //obtiene censo web

            //si la pagina actul es el login 
            if (url.endsWith(path + "/") || url.endsWith("/login.xhtml")) {
                //si hay una sesion iniciada redirige al index siempre y no permite ir al login

                if (user != null) {

                    if ("ADMINISTRADOR".equals(user.getRole())) {
                        context.redirect(path + "/administrador/index.xhtml");
                    } else {
                        context.redirect(path + "/supervisor/index.xhtml");
                    }

                    context.redirect("index.xhtml");
                }
            } else if (user == null)// si es otra pagina y no hay sesion iniciada abre un unauthorized
            {
                context.redirect(path + "/unauthorized.xhtml");
            } //perfiles 
            else { //es otra pagina pero si hay sesion activa, se valida el perfil
                boolean isValid = false;
                if ("ADMINISTRADOR".equals(user.getRole())) {
                    for (String page : Constants.LIST_ADMIN_PAGES) {
                        if (url.endsWith("/administrador/" + page + ".xhtml")) {
                            isValid = true;
                            break;
                        }
                    }
                } else { //supervisor 
                    for (String page : Constants.LIST_SUPER_PAGES) {
                        if (url.endsWith("/supervisor/" + page + ".xhtml")) {
                            isValid = true;
                            break;
                        }
                    }
                }

                if (!isValid) {
                    context.invalidateSession();
                    context.redirect(path + "/error_pages/403.xhtml");
                }
            }

        } catch (IOException e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }

    }
}
