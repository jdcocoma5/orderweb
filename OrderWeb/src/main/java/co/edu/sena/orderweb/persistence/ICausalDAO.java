
package co.edu.sena.orderweb.persistence;

import co.edu.sena.orderweb.model.Causal;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface ICausalDAO {
    public void insert(Causal causal) throws Exception;
    public void update(Causal causal) throws Exception;
    public void delete(Causal causal) throws Exception;
    public Causal findById(Integer idCausal) throws Exception;
    public List<Causal> findAll() throws Exception;
}
