/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Observation;
import co.edu.sena.orderweb.persistence.IObservationDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class ObservationBean implements ObservationBeanLocal {
    @EJB
    private IObservationDAO observationDAO;

    public void validate(Observation observation) throws Exception
    {
        if(observation == null)
        {
            throw new Exception("La observación es nula");
        }
        
        if(observation.getDescription().isEmpty())
        {
            throw new Exception("La descripción es obligatoria");
        }
    }
    
    @Override
    public void insert(Observation observation) throws Exception {
        validate(observation);  
        observationDAO.insert(observation);
    }

    @Override
    public void update(Observation observation) throws Exception {
        validate(observation);
        Observation oldObservation= observationDAO.findById(observation.getIdObservation());
        if(oldObservation == null)
        {
            throw new Exception("No existe una observación con ese Id");
        }
        
        oldObservation.setDescription(observation.getDescription()); 
        observationDAO.update(oldObservation);
    }

    @Override
    public void delete(Observation observation) throws Exception {
        if(observation == null)
        {
            throw new Exception("La observación es nula");
        }
        
        if(observation.getIdObservation() == 0)
        {
            throw new Exception("El Id es obligatorio");
        }        
        
        Observation oldCausal= observationDAO.findById(observation.getIdObservation());
        if(oldCausal == null)
        {
            throw new Exception("No existe una observación con ese Id");
        }
        
        observationDAO.delete(oldCausal);
    }

    @Override
    public Observation findById(Integer idObservation) throws Exception {
        if(idObservation == 0)
        {
            throw new Exception("El Id es obligatorio");
        } 
        
        return observationDAO.findById(idObservation);
    }

    @Override
    public List<Observation> findAll() throws Exception {
        return observationDAO.findAll();
    }
    
    
    
}
