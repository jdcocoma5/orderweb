/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.utils;

/**
 *
 * @author Aprendiz
 */
public final class Constants {
    public static final String[] LIST_ADMIN_PAGES ={
        "index",
        "causal",
        "observation",
        "type_activity"
    };
    public static final String[] LIST_SUPER_PAGES ={
        "index",
        "technician",
        "activity",
        "order",
    };
}
