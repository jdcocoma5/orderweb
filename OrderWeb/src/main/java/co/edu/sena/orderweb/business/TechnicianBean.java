/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Technician;
import co.edu.sena.orderweb.persistence.ITechnicianDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class TechnicianBean implements TechnicianBeanLocal {

    @EJB
    private ITechnicianDAO technicianDAO;
    
    public void validate(Technician technician) throws Exception            
    {
        if(technician == null)
        {
            throw new Exception("El técnico es nulo");
        }
        
        if(technician.getDocument() == 0)
        {
            throw new Exception("El documento es obligatorio");
        }  
        
        if(technician.getName().isEmpty())
        {
            throw new Exception("La fecha es obligatoria");
        }        
    }

    @Override
    public void insert(Technician technician) throws Exception {
        validate(technician);
        Technician oldTechnician = technicianDAO.findById(technician.getDocument());
        if(oldTechnician != null)
        {
            throw new Exception("Ya existe un técnico con el mismo documento");
        }
        
        technicianDAO.insert(technician);
    }

    @Override
    public void update(Technician technician) throws Exception {
        validate(technician);
        Technician oldTechnician = technicianDAO.findById(technician.getDocument());
        if(oldTechnician == null)
        {
            throw new Exception("No existe un técnico con ese documento");
        }
        
        oldTechnician.setEspeciality(technician.getEspeciality());
        oldTechnician.setName(technician.getName());
        oldTechnician.setPhone(technician.getPhone());
        
        technicianDAO.update(oldTechnician);
    }

    @Override
    public void delete(Technician technician) throws Exception {
        if(technician == null)
        {
            throw new Exception("El técnico es nulo");
        }
        
        if(technician.getDocument() == 0)
        {
            throw new Exception("El documento es obligatorio");
        }  
        
        Technician oldTechnician = technicianDAO.findById(technician.getDocument());
        if(oldTechnician == null)
        {
            throw new Exception("No existe un técnico con ese documento");
        }
        
        technicianDAO.delete(oldTechnician);
    }

    @Override
    public Technician findById(Long document) throws Exception {
        if(document == 0)
        {
            throw new Exception("El documento es obligatorio");
        } 
        
        return technicianDAO.findById(document);
    }

    @Override
    public List<Technician> findAll() throws Exception {
        return technicianDAO.findAll();
    }
    
    
}
