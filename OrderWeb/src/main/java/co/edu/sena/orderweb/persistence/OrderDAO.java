/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/ClassDAO.java to edit this template
 */

package co.edu.sena.orderweb.persistence;

import co.edu.sena.orderweb.model.Order1;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class OrderDAO implements IOrderDAO{
    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public void insert(Order1 order1) throws Exception {
        try {
            entityManager.persist(order1);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(Order1 order1) throws Exception {
        try {
            entityManager.merge(order1);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void delete(Order1 order1) throws Exception {
        try {
            entityManager.remove(order1);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public Order1 findById(Integer idOrder) throws Exception {
        try {
            return entityManager.find(Order1.class, idOrder);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public List<Order1> findAll() throws Exception {
        try {
            Query query =  entityManager.createNamedQuery("Order1.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}
