/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/SessionLocal.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.UserLogin;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface UserBeanLocal {
    public UserLogin findById(String username) throws Exception;
    public List<UserLogin> findAll() throws Exception;
    public UserLogin login(String role, String password) throws Exception;
}
