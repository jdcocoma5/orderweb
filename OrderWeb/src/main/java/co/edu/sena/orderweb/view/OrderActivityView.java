/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.orderweb.view;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author usuario
 */
@Named(value = "orderActivityView")
@RequestScoped
public class OrderActivityView {

    /**
     * Creates a new instance of OrderActivityView
     */
    public OrderActivityView() {
    }
    
}
