/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.orderweb.view;

import co.edu.sena.orderweb.business.TechnicianBeanLocal;
import co.edu.sena.orderweb.model.Technician;
import co.edu.sena.orderweb.utils.MessageUtils;
import java.util.List;
import javax.ejb.EJB;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author usuario
 */

public class TechnicianView {
    
    private InputText txtDocument;
    private InputText txtName;
    private InputText txtEspeciality;
    private InputText txtPhone;
    
    private List<Technician>listTechnician= null;
    
    private CommandButton btnCrear;
    private CommandButton btnModificar;
    private CommandButton btnEliminar;
    
    @EJB
    private TechnicianBeanLocal technicianBean;
    /**
     * Creates a new instance of TechnicianView
     */
    public TechnicianView() {
    }

    public InputText getTxtDocument() {
        return txtDocument;
    }

    public void setTxtDocument(InputText txtDocument) {
        this.txtDocument = txtDocument;
    }

    public InputText getTxtName() {
        return txtName;
    }

    public void setTxtName(InputText txtName) {
        this.txtName = txtName;
    }

    public InputText getTxtEspeciality() {
        return txtEspeciality;
    }

    public void setTxtEspeciality(InputText txtEspeciality) {
        this.txtEspeciality = txtEspeciality;
    }

    public InputText getTxtPhone() {
        return txtPhone;
    }

    public void setTxtPhone(InputText txtPhone) {
        this.txtPhone = txtPhone;
    }

    public List<Technician> getListTechnician() {
        if (listTechnician == null) {
            try {
                listTechnician = technicianBean.findAll();
            } catch (Exception e) {
                MessageUtils.addErrorMessage(e.getMessage());
            }
        }
        return listTechnician;
    }

    public void setListTechnician(List<Technician> listTechnician) {
        this.listTechnician = listTechnician;
    }

    public CommandButton getBtnCrear() {
        return btnCrear;
    }

    public void setBtnCrear(CommandButton btnCrear) {
        this.btnCrear = btnCrear;
    }

    public CommandButton getBtnModificar() {
        return btnModificar;
    }

    public void setBtnModificar(CommandButton btnModificar) {
        this.btnModificar = btnModificar;
    }

    public CommandButton getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(CommandButton btnEliminar) {
        this.btnEliminar = btnEliminar;
    }
    public void clear() {

        txtDocument.setValue("");
        txtName.setValue("");
        txtEspeciality.setValue("");
        txtPhone.setValue("");

       

        listTechnician = null;

        btnCrear.setDisabled(false);
        btnModificar.setDisabled(true);
        btnEliminar.setDisabled(true);

    }

    public void insert() {
        try {
            Technician technician = new Technician();

            technician.setDocument(Long.parseLong(txtDocument.getValue().toString()));
            technician.setEspeciality(txtEspeciality.getValue().toString());
            technician.setName(txtName.getValue().toString());
            technician.setPhone(txtPhone.getValue().toString());            
            
            technicianBean.insert(technician);
             MessageUtils.addInfoMessage("Tecnico creado exitosamente");
            clear();

        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }

    public void update() {
        try {
            Technician technician = new Technician();

            technician.setDocument(Long.parseLong(txtDocument.getValue().toString()));
            technician.setEspeciality(txtEspeciality.getValue().toString());
            technician.setName(txtName.getValue().toString());
            technician.setPhone(txtPhone.getValue().toString());            
            
            technicianBean.update(technician);
            MessageUtils.addInfoMessage("Tecnico modificado exitosamente");
            clear();

        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }

    public void delete() {
        try {
            Technician technician = new Technician();
            technician.setDocument(Long.parseLong(txtDocument.getValue().toString()));
            technicianBean.delete(technician);
            MessageUtils.addInfoMessage("Tecnico eliminado exitosamente");
            clear();

        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }

    public void onRowSelect(SelectEvent event) {

        Technician technician = (Technician) event.getObject();
        
        txtDocument.setValue(technician.getDocument());
        txtEspeciality.setValue(technician.getEspeciality());
        txtName.setValue(technician.getName());
        txtPhone.setValue(technician.getPhone());

       //BTN
        btnCrear.setDisabled(true);
        btnModificar.setDisabled(false);
        btnEliminar.setDisabled(false);
    }

    public void txtIdListener() {
        try {
            Technician technician = technicianBean.findById(Long.parseLong(txtDocument.getValue().toString()));

            if (technician != null) {
                
                txtEspeciality.setValue(technician.getEspeciality());
                txtName.setValue(technician.getName());
                txtPhone.setValue(technician.getPhone());

                btnCrear.setDisabled(true);
                btnModificar.setDisabled(false);
                btnEliminar.setDisabled(false);
                
            } else {
                                  
                txtEspeciality.setValue("");
                txtName.setValue("");
                txtPhone.setValue("");
                
                listTechnician = null;                   

                btnCrear.setDisabled(false);
                btnModificar.setDisabled(true);
                btnEliminar.setDisabled(true);

                MessageUtils.addInfoMessage("Tecnico no registrado");
            }
        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }
        
}
