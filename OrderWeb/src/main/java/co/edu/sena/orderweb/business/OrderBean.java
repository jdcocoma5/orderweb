/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Order1;
import co.edu.sena.orderweb.persistence.IOrderDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class OrderBean implements OrderBeanLocal {

    @EJB
    private IOrderDAO orderDAO;
    
    public void validate(Order1 order1) throws Exception            
    {
        if(order1 == null)
        {
            throw new Exception("La orden es nula");
        }
        
        if(order1.getIdOrder() == 0)
        {
            throw new Exception("El Id es obligatorio");
        }  
        
        if(order1.getLegalizationDate() == null)
        {
            throw new Exception("La fecha es obligatoria");
        }
        
        if(order1.getCity().isEmpty())
        {
            throw new Exception("La ciudad es obligatoria");
        }
        
        if(order1.getState().isEmpty())
        {
            throw new Exception("El estado/departamento son obligatorios");
        } 
    }

    @Override
    public void insert(Order1 order1) throws Exception {
        validate(order1);
        Order1 oldOrder1 = orderDAO.findById(order1.getIdOrder());
        if(oldOrder1 != null)
        {
            throw new Exception("Ya existe una orden con el mismo Id");
        }
        
        orderDAO.insert(order1);
    }

    @Override
    public void update(Order1 order1) throws Exception {
        validate(order1);
        Order1 oldOrder1 = orderDAO.findById(order1.getIdOrder());
        if(oldOrder1 == null)
        {
            throw new Exception("No existe una orden con ese Id");
        }
        
        oldOrder1.setAddress(oldOrder1.getAddress());
        oldOrder1.setCity(oldOrder1.getCity());
        oldOrder1.setIdCausal(oldOrder1.getIdCausal());
        oldOrder1.setIdObservation(oldOrder1.getIdObservation());
        oldOrder1.setLegalizationDate(oldOrder1.getLegalizationDate());  
        oldOrder1.setState(oldOrder1.getState());  
        
        orderDAO.update(oldOrder1);
    }

    @Override
    public void delete(Order1 order1) throws Exception {
        if(order1 == null)
        {
            throw new Exception("La orden es nula");
        }
        
        if(order1.getIdOrder() == 0)
        {
            throw new Exception("El Id es obligatorio");
        }
        
        Order1 oldOrder1 = orderDAO.findById(order1.getIdOrder());
        if(oldOrder1 == null)
        {
            throw new Exception("No existe una orden con ese Id");
        }
        
        orderDAO.delete(oldOrder1);
    }

    @Override
    public Order1 findById(Integer idOrder) throws Exception {
        if(idOrder == 0)
        {
            throw new Exception("El Id es obligatorio");
        }
        
        return orderDAO.findById(idOrder);
    }

    @Override
    public List<Order1> findAll() throws Exception {
        return orderDAO.findAll();
    }
    
    
}
