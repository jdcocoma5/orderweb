/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.persistence;

import co.edu.sena.orderweb.model.UserLogin;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class UserDAO implements IUserDAO{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public UserLogin findById(String username) throws Exception {
        try {
            return entityManager.find(UserLogin.class, username);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public List<UserLogin> findAll() throws Exception {
        try {
            Query query =  entityManager.createNamedQuery("UserLogin.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public UserLogin findByRole(String role) throws Exception {
        try {
            Query query =  entityManager.createQuery("select u from UserLogin u where u.role = :role")
                                                    .setParameter("role", role);
            return (UserLogin) query.getSingleResult();
        }catch (NoResultException e) {
            return null;
        } 
        catch (RuntimeException e) {
            throw e;
        }
    }
    
    
}
