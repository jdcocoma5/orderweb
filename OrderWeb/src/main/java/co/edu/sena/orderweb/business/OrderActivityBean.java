/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Activity;
import co.edu.sena.orderweb.model.Order1;
import co.edu.sena.orderweb.model.OrderActivity;
import co.edu.sena.orderweb.model.OrderActivityPK;
import co.edu.sena.orderweb.persistence.IActivityDAO;
import co.edu.sena.orderweb.persistence.IOrderActivityDAO;
import co.edu.sena.orderweb.persistence.IOrderDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class OrderActivityBean implements OrderActivityBeanLocal {

    @EJB
    private IOrderActivityDAO orderActivityDAO;
    @EJB
    private IOrderDAO orderDAO;
    @EJB
    private IActivityDAO activityDAO;
    
    public void validate(OrderActivity orderActivity) throws Exception
    {
        if(orderActivity == null)
        {
            throw new Exception("Orden Actividad es nulo");
        }
        
        if(orderActivity.getActivity() == null)
        {
            throw new Exception("La Actividad es obligatoria");
        }
        
        if(orderActivity.getOrder1() == null)
        {
            throw new Exception("La Orden es obligatoria");
        }
        
        if(orderActivity.getSerial() == 0)
        {
            throw new Exception("El consecutivo es obligatorio");
        }
    }

    @Override
    public void insert(OrderActivity orderActivity) throws Exception {
        validate(orderActivity);
        Activity oldActivity = activityDAO.findById(orderActivity.getActivity().getIdActivity());
        Order1 oldOrder1 = orderDAO.findById(orderActivity.getOrder1().getIdOrder());
        
        OrderActivityPK orderActivityPK = new OrderActivityPK();
        orderActivityPK.setIdActivity(oldActivity.getIdActivity());
        orderActivityPK.setIdOrder(oldOrder1.getIdOrder());
        //consulta que la combinacion order-activity no esté ya en la BD
        OrderActivity oldOrderActivity = orderActivityDAO.findById(
                            oldOrder1.getIdOrder(), oldActivity.getIdActivity());
        if(oldOrderActivity != null)
        {
            throw new Exception("Ya existe una orden con la misma actividad");
        }
        
        oldOrderActivity = new OrderActivity();
        oldOrderActivity.setActivity(oldActivity);
        oldOrderActivity.setOrder1(oldOrder1);
        oldOrderActivity.setOrderActivityPK(orderActivityPK);
        oldOrderActivity.setSerial(orderActivity.getSerial());
        
        orderActivityDAO.insert(oldOrderActivity);
    }

    @Override
    public void update(OrderActivity orderActivity) throws Exception {
        validate(orderActivity);
        Activity oldActivity = activityDAO.findById(orderActivity.getActivity().getIdActivity());
        Order1 oldOrder1 = orderDAO.findById(orderActivity.getOrder1().getIdOrder());
        
        OrderActivityPK orderActivityPK = new OrderActivityPK();
        orderActivityPK.setIdActivity(oldActivity.getIdActivity());
        orderActivityPK.setIdOrder(oldOrder1.getIdOrder());
        //consulta que la combinacion order-activity no esté ya en la BD
        OrderActivity oldOrderActivity = orderActivityDAO.findById(
                            oldOrder1.getIdOrder(), oldActivity.getIdActivity());
        if(oldOrderActivity == null)
        {
            throw new Exception("No existe una orden con esa actividad");
        }        
        
        oldOrderActivity.setActivity(oldActivity);
        oldOrderActivity.setOrder1(oldOrder1);
        oldOrderActivity.setOrderActivityPK(orderActivityPK);
        oldOrderActivity.setSerial(orderActivity.getSerial());
        
        orderActivityDAO.update(oldOrderActivity);
    }

    @Override
    public void delete(OrderActivity orderActivity) throws Exception {
        if(orderActivity == null)
        {
            throw new Exception("Orden Actividad es nulo");
        }
        
        //consulta que la combinacion order-activity no esté ya en la BD
        OrderActivity oldOrderActivity = orderActivityDAO.findById(
            orderActivity.getOrder1().getIdOrder(), orderActivity.getActivity().getIdActivity());
        if(oldOrderActivity == null)
        {
            throw new Exception("No existe una orden con esa actividad");
        } 
        
        orderActivityDAO.delete(oldOrderActivity);
    }

    @Override
    public OrderActivity findById(Integer idOrder, Integer idActivity) throws Exception {
        if(idOrder == 0 || idActivity == 0)
        {
            throw new Exception("El Id de la orden y de la actividad son obligatorios");
        }
        
        return orderActivityDAO.findById(idOrder, idActivity);
    }

    @Override
    public List<OrderActivity> findAll() throws Exception {
        return orderActivityDAO.findAll();
    }
    
    
}
