/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.orderweb.persistence;


import co.edu.sena.orderweb.model.Observation;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface IObservationDAO {
    public void insert(Observation observation) throws Exception;
    public void update(Observation observation) throws Exception;
    public void delete(Observation observation) throws Exception;
    public Observation findById(Integer idObservation) throws Exception;
    public List<Observation> findAll() throws Exception;
}
